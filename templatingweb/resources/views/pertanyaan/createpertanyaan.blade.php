@extends('adminlte.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" actions = "/posts" method = "POST" >
            <div class="card-body">
                <div class="form-group">
                <label for="inputJudul">Judul</label>
                <input type="text" class="form-control" id="inputJudul" placeholder="Masukkan Judul">
                </div>
                <div class="form-group">
                <label for="inputIsi">Isi Pertanyaan</label>
                <input type="text" class="form-control" id="inputIsi" placeholder="Isi">
                </div>
            </div>
            <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
@endsection
