@extends('adminlte.master')

@section('content')
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Daftar Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif
        <a class="btn btn-primary mb-2" href="/pertanyaan/create">Buat Pertanyaan</a>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 10px">No</th>
                <th>Judul</th>
                <th>Isi</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse($pertanyaan as $key => $pertanyaann)
                    <tr>
                        <td> {{$key = 1}}</td>
                        <td>{{$pertanyaann->judul}}</td>
                        <td>{{$pertanyaann->isi}}</td>
                        <td style="display: flex">
                            <a href="/pertanyaan/{{$pertanyaann->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/pertanyaan/{{$pertanyaann->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                            <form action="/pertanyaan/{{$pertanyaann->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" name="delete" value="delete" class="btn btn-danger btn-sm">

                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                    </tr>
                @endforelse
            </tr>
            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
        </div> -->
    </div>

@endsection
