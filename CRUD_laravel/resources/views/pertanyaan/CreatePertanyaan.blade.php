@extends('adminlte.master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role=form method="POST" action= "/pertanyaan">
        @csrf
        <!-- @method("POST") -->
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" name="judul" value="{{ old('judul', '') }}" class="form-control" id="judul" placeholder="Masukkan Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                </div>

                <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <input type="text" class="form-control" value="{{ old('isi', '') }}" name="isi" id="isi" placeholder="Isi">
                    @error('isi')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>

            </div>
            <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
@endsection
