<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.CreatePertanyaan');
    }
    public function store(request $request){

        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
                "judul" => $request["judul"],
                "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Terupload!');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan') -> get();
        // dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));

    }


    public function show($pertanyaan_id) {
        $pertanyaan = DB::table('pertanyaan')->where('id',$pertanyaan_id)->first();
        // dd($pertanyaan);
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function edit($pertanyaan_id) {
        $pertanyaan = DB::table('pertanyaan')->where('id',$pertanyaan_id)->first();
        // dd($pertanyaan);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($pertanyaan_id, request $request) {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        $update = DB::table('pertanyaan')
        ->where('id', $pertanyaan_id)
        ->update([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Terupload!');
    }

    public function destroy($pertanyaan_id) {
        $deletess = DB::table('pertanyaan')->where('id',$pertanyaan_id)->delete();
        // dd($pertanyaan);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus!');
    }
}
