<!DOCTYPE html>
<html>
    <head>
        <title>Form Sign Up</title>
        <style>
            .button {
              border: none;
              color: white;
              padding: 10px 21px;
              text-align: center;
              text-decoration: none;
              display: inline-block;
              font-size: 13px;
              margin: 4px 2px;
              cursor: pointer;
            }

            .signup {background-color: #7e959c;}
            </style>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label for="fname">First Name</label><br>
            <br>
            <input type="text" id="fname" name="fname"><br>
            <br>
            <label for="lname">Last Name</label><br>
            <br>
            <input type="text" id="lname" name="lname"><br>


        <p>Gender:</p>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male"> Male </label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label><br>
        <br>
            <label for="nationality">Nationality</label><br>
            <br>
            <select id="nationality" name="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="foreign">foreign</option>
            </select>


        <p>Language Spoken</p>
            <input type="checkbox" id="indonesia" name="lang" value="indonesoa">
            <label for="indonesia"> Bahasa indonesia</label><br>
            <input type="checkbox" id="English" name="lang" value="english">
            <label for="english">English</label><br>
            <input type="checkbox" id="other" name="lang" value="other">
            <label for="other">Other</label><br>
        <br>
        <label for="bio">Bio:</label><br>
        <br>
        <textarea id="Bio" name="Bio" rows="10" cols="40">

        </textarea>
        <br>
        <input type="submit" value="REGISTER">

        </form>

    </body>



</html>
