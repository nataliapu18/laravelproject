<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('register');
    }

    public function welcome(Request $request) {
        $first = $request -> fname;
        $last = $request -> lname;
        return view('welcome', compact('first','last'));
    }
}
